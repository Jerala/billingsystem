import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DetailComponent} from './modules/detail/detail.component';
import {HomeComponent} from './modules/home/home.component';
import {DiscountComponent} from './modules/discount/discount.component';
import {TariffComponent} from './modules/tariff/tariff.component';
import {PaymentComponent} from './modules/payment/payment.component';
import {LoginComponent} from './modules/login/login.component';
import {CustomerInfoComponent} from './modules/customer-info/customer-info.component';
import {OnlyLoggedInUsersGuard} from './only-logged-in-users.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'home', redirectTo: '/home/customer-info', pathMatch: 'full'},
  {
    path: 'home',
    component: HomeComponent,
    canActivate: [OnlyLoggedInUsersGuard],
    canActivateChild: [OnlyLoggedInUsersGuard],
    children: [
      {path: 'customer-info', component: CustomerInfoComponent},
      {path: 'detail', component: DetailComponent},
      {path: 'discounts', component: DiscountComponent},
      {path: 'tariffs', component: TariffComponent},
      {path: 'payment', component: PaymentComponent}
    ]
  },
  {path: '', redirectTo: '/home/customer-info', pathMatch: 'full'},
  {path: '**', redirectTo: '/home/customer-info'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}

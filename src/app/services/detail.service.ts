import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import {Observable} from 'rxjs';
import {DetailModel} from '../models/detail.model';
import {BACKEND_URL} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(private http: HttpClient) {
  }

  public getDetails(): Observable<DetailModel[]> {
    const url = BACKEND_URL + '/billing/bills';
    return this.http.get<DetailModel[]>(url);
  }

  public getPdf(serviceType: string, dateFrom: number, dateTo: number) {
    let url;
    if (serviceType != null) {
      url = BACKEND_URL + '/billing/detalization' + '?access_token='
        + localStorage.getItem('access_token') + '&service_type=' + serviceType
        + '&date_from=' + dateFrom + '&date_to=' + dateTo;
    } else {
      url = BACKEND_URL + '/billing/invoice' + '?access_token='
        + localStorage.getItem('access_token') + '&date_from=' + dateFrom + '&date_to=' + dateTo;
    }
    window.open(url, '_blank');
  }
}

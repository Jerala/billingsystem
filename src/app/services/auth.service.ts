import {Injectable} from '@angular/core';
import {catchError, map, share} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TokensModel} from '../models/tokens.model';
import {BACKEND_URL} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient,
              private decoder: JwtHelperService,
              private router: Router) {
  }

  public login(username: string, password: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + btoa('devglan-client:devglan-secret')
    });
    const params = new HttpParams()
      .set('username', username)
      .set('password', password)
      .set('grant_type', 'password');
    return this.http.post(
      BACKEND_URL + '/oauth/token', params, {headers: headers})
      .pipe(map((tokens: TokensModel) => {
        if (tokens.access_token && tokens.refresh_token) {
          localStorage.setItem('access_token', tokens.access_token);
          localStorage.setItem('refresh_token', tokens.refresh_token);
        }
      }), catchError(error => {
        return throwError(error);
      }));
  }

  public logout(): void {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
  }

  public refreshToken(): Observable<string> {
    const url = BACKEND_URL + '/oauth/token';

    const refreshToken = localStorage.getItem('refresh_token');
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + btoa('devglan-client:devglan-secret')
    });
    const params = new HttpParams()
      .set('grant_type', 'refresh_token')
      .set('refresh_token', refreshToken);
    return this.http
      .post(url, params, {headers: headers})
      .pipe(
        share(),
        map((res: TokensModel) => {
          const accessToken = res.access_token;
          const newRefreshToken = res.refresh_token;
          localStorage.clear();
          localStorage.setItem('refresh_token', newRefreshToken);
          localStorage.setItem('access_token', accessToken);

          return accessToken;
        }),
        catchError(error => {
          localStorage.clear();
          this.router.navigate(['/login']);
          return throwError(error);
        })
      );
  }

  public getToken(): Observable<string> {
    const token = localStorage.getItem('access_token');

    const isTokenExpired = this.decoder.isTokenExpired(token);
    if (isTokenExpired) {
      return this.refreshToken();
    }
    return of(token);
  }
}

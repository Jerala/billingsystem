import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError, switchMap} from 'rxjs/operators';
import {AuthService} from './auth.service';
import {BACKEND_URL} from '../defaults';

@Injectable()
export class HttpAuthInterceptor implements HttpInterceptor {

  private inflightAuthRequest: Observable<string> = null;

  constructor(private authService: AuthService) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (BACKEND_URL.includes('3000')) {
      req = req.clone({
        method: 'GET'
      });
    }
    if (req.url.endsWith('oauth/token')) {
      return next.handle(req);
    }

    if (!this.inflightAuthRequest) {
      this.inflightAuthRequest = this.authService.getToken();
    }
    return this.inflightAuthRequest.pipe(
      switchMap((newToken: string) => {
          this.inflightAuthRequest = null;
          const authReq = req.clone({
            headers: req.headers.set('Authorization', newToken ? 'Bearer ' + newToken : '')
          });
          return next.handle(authReq);
        },
      ),
      catchError((error) => {
        this.inflightAuthRequest = null;
        return throwError(error);
      }));
  }
}

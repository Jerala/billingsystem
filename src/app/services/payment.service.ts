import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {PaymentModel} from '../models/payment.model';
import {BACKEND_URL} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) {
  }

  public getPayments(): Observable<PaymentModel[]> {
    const url = BACKEND_URL + '/payments';
    return this.http.get<PaymentModel[]>(url);
  }

  public addPayment(paymentValue: number): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const params = new HttpParams()
      .set('payment', paymentValue.toString());
    const url = BACKEND_URL + '/customer/balance/update';
    return this.http.post<string>(url, params, {headers: headers});
  }

}

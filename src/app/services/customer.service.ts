import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {CustomerModel} from '../models/customer.model';
import {BACKEND_URL} from '../defaults';
import {CustomerNameModel} from '../models/customer-name.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private balance = new Subject<number>();
  private remainingSMS = new Subject<number>();
  private remainingSeconds = new Subject<number>();
  private remainingMegabytes = new Subject<number>();

  constructor(private http: HttpClient) {
  }

  public getCustomer(): Observable<CustomerModel> {
    const url = BACKEND_URL + '/customer/info';
    return this.http.get<CustomerModel>(url);
  }

  public getCustomerName(): Observable<CustomerNameModel> {
    const url = BACKEND_URL + '/customer/name';
    return this.http.get<CustomerNameModel>(url);
  }

  public updateBalance(): void {
    const url = BACKEND_URL + '/customer/balance';
    this.http.get<number>(url)
      .subscribe((balance) => {
        balance = Math.round(balance * 100) / 100;
        this.balance.next(balance);
      });
  }

  public getServiceId(): Observable<number> {
    const url = BACKEND_URL + '/customer/serviceId';
    return this.http.get<number>(url);
  }

  public currentBalance(): Observable<number> {
    return this.balance;
  }

  public getRemainingSMS(): Observable<number> {
    return this.remainingSMS;
  }

  public setRemainingSMS(remainingSMS: number): void {
    this.remainingSMS.next(remainingSMS);
  }

  public getRemainingSeconds(): Observable<number> {
    return this.remainingSeconds;
  }

  public setRemainingSeconds(remainingSeconds: number): void {
    this.remainingSeconds.next(remainingSeconds);
  }

  public getRemainingMegabytes(): Observable<number> {
    return this.remainingMegabytes;
  }

  public setRemainingMegabytes(remainingMegabytes: number): void {
    this.remainingMegabytes.next(remainingMegabytes);
  }

  public updateCustomer(email: string): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const params = new HttpParams()
      .set('email', email);
    const url = BACKEND_URL + '/customer/update';
    return this.http.post<string>(url, params, {headers: headers});
  }

  public changeTariff(tariffName: string): Observable<string> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    const params = new HttpParams()
      .set('tariffName', tariffName);
    const url = BACKEND_URL + '/customer/update/tariff';
    return this.http.post<string>(url, params, {headers: headers});
  }

  public getTariffName(): Observable<any> {
    const url = BACKEND_URL + '/customer/tariffName';
    return this.http.get<any>(url);
  }
}

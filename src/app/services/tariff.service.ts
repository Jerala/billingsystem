import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TariffModel} from '../models/tariff.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BACKEND_URL} from '../defaults';

@Injectable({
  providedIn: 'root'
})
export class TariffService {

  constructor(private http: HttpClient) {
  }

  public getTariffs(): Observable<TariffModel[]> {
    const url = BACKEND_URL + '/tariffs';
    return this.http.get<TariffModel[]>(url);
  }

  public getTariffById(id: number): Observable<TariffModel> {
    const url = BACKEND_URL + '/tariff/info';
    const params = new HttpParams()
      .set('id', id.toString());
    return this.http.get<TariffModel>(url, { params: params });
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortCustomerInfoComponent } from '../modules/home/short-customer-info/short-customer-info.component';

describe('ShortCustomerInfoComponent', () => {
  let component: ShortCustomerInfoComponent;
  let fixture: ComponentFixture<ShortCustomerInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortCustomerInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortCustomerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

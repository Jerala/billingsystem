export interface CustomerModel {
  serviceId: number;
  personalAccount: number;
  firstName: string;
  patronymic: string;
  lastName: string;
  region: string;
  tariffId: number;
  creationDate: Date;
  birthday: Date;
  email: string;
  additionalServiceId: number;
  limits: {
    remainingMegabytes: number;
    remainingSMS: number;
    remainingSeconds: number;
    tariffDiscount: number;
  }
}

export interface NotificationModel {
  serviceId: number;
  total: number;
  serviceType: string;
  remainingValue: number;
}

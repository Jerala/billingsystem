export interface PaymentModel {
  id: number;
  serviceId: number;
  paymentDate: Date;
  payment: number;
  balanceAfterPayment: number;
}

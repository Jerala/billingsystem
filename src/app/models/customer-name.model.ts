export interface CustomerNameModel {
    firstName: string;
    patronymic: string;
    lastName: string;
    middleName: string;
}

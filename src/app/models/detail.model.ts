export interface DetailModel {
  id: number;
  description: string;
  creationDate: Date;
  currencyCode: string;
  status: string;
  subtotal: number;
  discount: number;
  total: number;
  serviceType: string;
}

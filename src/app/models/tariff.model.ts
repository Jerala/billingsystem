export interface TariffModel {
  name: string;
  description: string;
  monthlyPrice: number;
  freeSeconds: number;
  freeSMS: number;
  freeMegabytes: number;
  pricePerMinute: number;
  pricePerSMS: number;
  pricePerMegabyte: number;
  discount: number;
}

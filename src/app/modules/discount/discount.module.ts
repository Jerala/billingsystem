import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DiscountComponent} from './discount.component';

@NgModule({
  declarations: [
    DiscountComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DiscountComponent
  ]
})
export class DiscountModule {
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CustomerInfoComponent} from './customer-info.component';
import { ClarityModule } from '@clr/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from 'src/app/pipes/pipe.module';
import {AppRoutingModule} from '../../app-routing.module';
import {NgCircleProgressModule} from 'ng-circle-progress';

@NgModule({
  declarations: [
    CustomerInfoComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    ClarityModule,
    FormsModule,
    ReactiveFormsModule,
    PipeModule,
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 100,
      outerStrokeWidth: 16,
      innerStrokeWidth: 8,
      outerStrokeColor: "#78C000",
      innerStrokeColor: "#C7E596",
      animationDuration: 300,
      renderOnClick: false
    })
  ]
})
export class CustomerInfoModule {
}

import {Component, OnInit} from '@angular/core';
import {CustomerService} from '../../services/customer.service';
import {CustomerModel} from '../../models/customer.model';
import {TariffModel} from '../../models/tariff.model';
import {TariffService} from '../../services/tariff.service';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.css']
})
export class CustomerInfoComponent implements OnInit {

  /** @internal */
  public _customer: CustomerModel;
  /** @internal */
  public _customerTariff: TariffModel;
  /** @internal */
  public _email: string;
  /** @internal */
  public _isAlertVisible = false;
  /** @internal */
  public _isEditMode = false;
  /** @internal */
  public _isEmailIncorrect = false;
  /** @internal */
  public _megabytesRemainingPercent: number;
  /** @internal */
  public _SMSRemainingPercent: number;
  /** @internal */
  public _SecondsRemainingPercent: number;
  private alertTimeoutFunc: number;

  constructor(private customerService: CustomerService,
              private tariffService: TariffService) {
    this.customerService.getRemainingMegabytes()
      .subscribe((megabytes) => {
        this._customer.limits.remainingMegabytes = megabytes;
        this._megabytesRemainingPercent =
          this._customer.limits.remainingMegabytes / this._customerTariff.freeMegabytes * 100;
      });
    this.customerService.getRemainingSeconds()
      .subscribe((seconds) => {
        this._customer.limits.remainingSeconds = seconds;
        this._SecondsRemainingPercent =
          this._customer.limits.remainingSeconds / this._customerTariff.freeSeconds * 100;
      });
    this.customerService.getRemainingSMS()
      .subscribe((sms) => {
        this._customer.limits.remainingSMS = sms;
        this._SMSRemainingPercent =
          this._customer.limits.remainingSMS / this._customerTariff.freeSMS * 100;
      });
  }

  ngOnInit() {
    this.getCustomer();
  }

  private getCustomer(): void {
    this.customerService.getCustomer()
      .subscribe(customer => {
        this._customer = customer;
        this._customer.birthday = new Date(1990, 1, 31);
        this._email = customer.email;
        this.getCustomerTariff(customer.tariffId);
      });
  }

  private getCustomerTariff(id: number): void {
    this.tariffService.getTariffById(id)
      .subscribe((tariff) => {

        this._customerTariff = tariff;

        this._customerTariff.pricePerMegabyte =
          Math.round(this._customerTariff.pricePerMegabyte * 100) / 100;

        this._megabytesRemainingPercent =
          this._customer.limits.remainingMegabytes / this._customerTariff.freeMegabytes * 100;

        this._SMSRemainingPercent =
          this._customer.limits.remainingSMS / this._customerTariff.freeSMS * 100;

        this._SecondsRemainingPercent =
          this._customer.limits.remainingSeconds / this._customerTariff.freeSeconds * 100;
      });
  }

  /** @internal */
  public _updateCustomerInfo(): void {
    if (!this._isEditMode) {
      return;
    }
    const emailRegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegExp.test(String(this._email).toLowerCase())) {
      this._isEmailIncorrect = true;
      return;
    }
    this._isEmailIncorrect = false;
    this.customerService.updateCustomer(this._email)
      .subscribe((result) => {
        if (result['result'] === 'success') {
          this._isEditMode = false;
          this.showSuccessAlert();
          this.getCustomer();
        }
      });
  }

  /** @internal */
  public _activateEditMode(): void {
    this._isEditMode = !this._isEditMode;
    this._email = this._customer.email;
    this._isEmailIncorrect = false;
  }

  private showSuccessAlert(): void {
    this._isAlertVisible = true;
    if (this.alertTimeoutFunc) {
      window.clearTimeout(this.alertTimeoutFunc);
    }
    this.alertTimeoutFunc = window.setTimeout(() => {
      this._isAlertVisible = false;
    }, 5000);
  }


}

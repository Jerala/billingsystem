import {Component, OnInit} from '@angular/core';
import { CustomerNameModel } from 'src/app/models/customer-name.model';
import { CustomerService } from 'src/app/services/customer.service';
import {NotificationModel} from '../../models/notification.model';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import {BACKEND_URL} from '../../defaults';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private serverUrl = BACKEND_URL + '/notifications/?access_token='
    + localStorage.getItem('access_token');
  private stompClient;
  private timeoutFunc: number;
  /** @internal */
  public _notification: NotificationModel;
  /** @internal */
  public _isAlertVisible = false;
  /** @internal */
  public _customerName: CustomerNameModel;
  /** @internal */
  public _balance: number;
  /** @internal */
  public _serviceId: number;

  constructor(private customerService: CustomerService) {
    this.initializeWebSocketConnection();
    this.customerService.currentBalance()
      .subscribe((balance) => this._balance = balance);
  }

  ngOnInit() {
    this.getCustomerName();
    this.getBalance();
    this.getServiceId();
  }

  public getBalance() {
    this.customerService.updateBalance();
  }

  public getCustomerName(): void {
    this.customerService.getCustomerName()
      .subscribe(customerName => this._customerName = customerName);
  }

  public getServiceId(): void {
    this.customerService.getServiceId()
      .subscribe((serviceId) => this._serviceId = serviceId);
  }

  private initializeWebSocketConnection(): void {
    const ws = new SockJS(this.serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;

    this.stompClient.connect({}, function (frame) {
      that.stompClient.subscribe('/user/queue/reply', (message) => {
        that._notification = JSON.parse(message.body);
        that._notification.total = Math.round(that._notification.total * 10) / 10;
        that.customerService.updateBalance();
        that.updateRemainingValue();
        that.showInfoAlert();
      });
    });
  }

  private showInfoAlert(): void {
    this._isAlertVisible = true;
    if (this.timeoutFunc) {
      window.clearTimeout(this.timeoutFunc);
    }
    this.timeoutFunc = window.setTimeout(() => {
      this._isAlertVisible = false;
    }, 30000);
  }

  private updateRemainingValue(): void {
    if (this._notification.serviceType === 'SMS') {
      this.customerService.setRemainingSMS(this._notification.remainingValue);
    } else if (this._notification.serviceType === 'VOICE') {
      this.customerService.setRemainingSeconds(this._notification.remainingValue);
    } else {
      this.customerService.setRemainingMegabytes(this._notification.remainingValue);
    }
  }

  /** @internal */
  public _closeAlert(): void {
    if (this.timeoutFunc) {
      this._isAlertVisible = false;
      window.clearTimeout(this.timeoutFunc);
    }
  }

}

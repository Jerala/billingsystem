import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeComponent} from './home.component';
import {CustomerInfoModule} from '../customer-info/customer-info.module';
import {AppRoutingModule} from '../../app-routing.module';
import {DetailModule} from '../detail/detail.module';
import {DiscountModule} from '../discount/discount.module';
import {PaymentModule} from '../payment/payment.module';
import {TariffModule} from '../tariff/tariff.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipeModule } from 'src/app/pipes/pipe.module';
import {ClarityModule} from '@clr/angular';
import {FooterModule} from '../footer/footer.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    FooterModule,
    CommonModule,
    AppRoutingModule,
    CustomerInfoModule,
    DetailModule,
    DiscountModule,
    PaymentModule,
    TariffModule,
    ReactiveFormsModule,
    FormsModule,
    ClarityModule,
    PipeModule
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {
}

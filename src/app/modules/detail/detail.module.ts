import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DetailComponent} from './detail.component';
import {ClarityModule} from '@clr/angular';
import {FooterModule} from '../footer/footer.module';

@NgModule({
  declarations: [
    DetailComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FooterModule
  ],
  exports: [
    DetailComponent
  ]
})
export class DetailModule {
}

import {Component, OnInit} from '@angular/core';

import {DetailModel} from '../../models/detail.model';
import {DetailService} from '../../services/detail.service';

enum Type {
  Sms = "SMS",
  Voice = "VOICE",
  Traffic = "TRAFFIC"
}

enum Details {
  Total = "total",
  Subtotal = "subtotal",
  Discount = "discount"
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  details: DetailModel[] = [];
  smsSubtotal: number;
  smsDiscount: number;
  smsTotal: number;
  voiceSubtotal: number;
  voiceDiscount: number;
  voiceTotal: number;
  trafficSubtotal: number;
  trafficDiscount: number;
  trafficTotal: number;
  dateTo: Date;
  dateFrom: Date;

  constructor(private detailService: DetailService) {
  }

  ngOnInit() {
    this.getDetails();
  }

  public getCurrencyCode(): string {
    return '$';
  }

  private getDetails(): void {
    this.detailService.getDetails()
      .subscribe(details => {
        this.details = details;
        this.smsSubtotal = this.get(Details.Subtotal, Type.Sms);
        this.smsDiscount = this.get(Details.Discount, Type.Sms);
        this.smsTotal = this.get(Details.Total, Type.Sms);
        this.voiceSubtotal = this.get(Details.Subtotal, Type.Voice);
        this.voiceDiscount = this.get(Details.Discount, Type.Voice);
        this.voiceTotal = this.get(Details.Total, Type.Voice);
        this.trafficSubtotal = this.get(Details.Subtotal, Type.Traffic);
        this.trafficDiscount = this.get(Details.Discount, Type.Traffic);
        this.trafficTotal = this.get(Details.Total, Type.Traffic);
      });
  }

  public get(type: string, serviceType: string): number {
    let sum = 0;
    this.details.filter(detail => detail.serviceType === serviceType)
      .forEach((detail) => sum += detail[type]);
    return Number(sum.toFixed(1));
  }

  public getPdf(serviceType: string) {
    this.detailService.getPdf(serviceType, this.dateFrom.getTime(), this.dateTo.getTime());
  }
}

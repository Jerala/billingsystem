import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TariffComponent} from './tariff.component';
import {ClarityModule} from '@clr/angular';

@NgModule({
  declarations: [
    TariffComponent
  ],
  imports: [
    CommonModule,
    ClarityModule
  ],
  exports: [
    TariffComponent
  ]
})
export class TariffModule {
}

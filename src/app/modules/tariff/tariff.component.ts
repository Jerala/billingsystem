import {Component, OnInit} from '@angular/core';
import {TariffService} from '../../services/tariff.service';
import {TariffModel} from '../../models/tariff.model';
import {CustomerService} from '../../services/customer.service';

@Component({
  selector: 'app-tariffs',
  templateUrl: './tariff.component.html',
  styleUrls: ['./tariff.component.css']
})
export class TariffComponent implements OnInit {

  /** @internal */
  public _tariffs: TariffModel[];
  /** @internal */
  public _isConfirmModalVisible = false;
  /** @internal */
  public _isErrorModalVisible = false;
  /** @internal */
  public _isSuccessModalVisible = false;
  /** @internal */
  public _tariffNameToChange: string;
  /** @internal */
  public _tariffToChangePrice: number;
  /** @internal */
  public _currentTariffName: string;
  private balance: number;

  constructor(private tariffService: TariffService,
              private customerService: CustomerService) {
    this.customerService.currentBalance()
      .subscribe((balance) => this.balance = balance);

  }

  ngOnInit() {
    this.getTariffs();
    this.getCurrentTariffName();
  }

  private getCurrentTariffName(): void {
    this.customerService.getTariffName()
      .subscribe((tariffName) => {
        this._currentTariffName = tariffName['tariffName'];
      });
  }

  private getTariffs(): void {
    this.tariffService.getTariffs()
      .subscribe((tariffs) =>  {
        this._tariffs = tariffs;
        for (let i = 0; i < this._tariffs.length; i++) {

          this._tariffs[i].pricePerMegabyte =
            Math.round(this._tariffs[i].pricePerMegabyte * 100) / 100;
        }
      });
  }

  /** @internal */
  public _changeTariff(): void {
    this._isConfirmModalVisible = false;
    if (this.balance < this._tariffToChangePrice) {
      this._isErrorModalVisible = true;
      return;
    }
    this.customerService.changeTariff(this._tariffNameToChange)
      .subscribe((result) => {
        if (result['result'] === 'success') {
          this._isSuccessModalVisible = true;
          this._currentTariffName = this._tariffNameToChange;
          this.customerService.updateBalance();
        } else {
          this._isErrorModalVisible = true;
        }
      })
  }

}

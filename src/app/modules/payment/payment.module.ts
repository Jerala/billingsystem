import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PaymentComponent} from './payment.component';
import {ClarityModule} from '@clr/angular';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PipeModule} from '../../pipes/pipe.module';

@NgModule({
  declarations: [
    PaymentComponent
  ],
  imports: [
    CommonModule,
    ClarityModule,
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    PipeModule
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentModule {
}


import {Component, OnInit} from '@angular/core';
import {PaymentModel} from '../../models/payment.model';
import {PaymentService} from '../../services/payment.service';
import {CustomerService} from '../../services/customer.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  /** @internal */
  public _payments: PaymentModel[];
  /** @internal */
  public _paymentValue = 300;
  /** @internal */
  public _balance: number;
  /** @internal */
  public _serviceId: number;
  /** @internal */
  public _lastPaymentDate: Date;
  /** @internal */
  public _isPaymentButtonDisabled = false;
  /** @internal */
  public _isModalVisible = false;
  /** @internal */
  public _repeatedPaymentValue?: number;
  /** @internal */
  public _isAlertVisible = false;
  private timeoutFunc: number;

  constructor(private paymentService: PaymentService,
              private userService: CustomerService) {
    this.userService.currentBalance()
      .subscribe((balance) => this._balance = balance);
  }

  ngOnInit() {
    this.updateBalance();
    this.getServiceId();
    this.getPayments();
  }

  private updateBalance(): void {
    this.userService.updateBalance();
  }

  private getServiceId(): void {
    this.userService.getServiceId()
      .subscribe((serviceId) => {
        this._serviceId = serviceId;
      });
  }

  private getPayments(): void {
    this.paymentService.getPayments()
      .subscribe((payments) => {
        this._payments = payments;
        this._lastPaymentDate = this._payments[this._payments.length - 1].paymentDate;
        this._payments.sort((a, b) => {
            if (a.paymentDate < b.paymentDate) {
              return 1;
            } else if (a.paymentDate > b.paymentDate) {
              return -1;
            } else {
              return 0;
            }
          }
        );
      });
  }

  /** @internal */
  public _addPayment(): void {
    const valueToPay = this._repeatedPaymentValue ? this._repeatedPaymentValue
      : this._paymentValue;
    this.paymentService.addPayment(valueToPay)
      .subscribe((result) => {
        if (result['result'] === 'success') {
          this.getPayments();
          this.updateBalance();
          this.showSuccessAlert();
          this._paymentValue = null;
          this._repeatedPaymentValue = null;
          this._isPaymentButtonDisabled = true;
          this._isModalVisible = false;
        }
      });
  }

  /** @internal */
  public _checkPaymentValue(): void {
    if (this._paymentValue < 15 || !this._paymentValue || this._paymentValue > 15000) {
      this._isPaymentButtonDisabled = true;
    } else {
      this._isPaymentButtonDisabled = false;
    }
  }

  /** @internal */
  public _showModal(): void {
    this._isModalVisible = true;
  }

  private showSuccessAlert(): void {
    this._isAlertVisible = true;
    if (this.timeoutFunc) {
      window.clearTimeout(this.timeoutFunc);
    }
    this.timeoutFunc = window.setTimeout(() => {
      this._isAlertVisible = false;
    }, 5000);
  }
}

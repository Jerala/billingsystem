import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CustomerService} from '../../services/customer.service';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() public username = '79255678912';
  @Input() public password: string;
  public failed: boolean;

  constructor(private router: Router,
              private customerService: CustomerService,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.authService.logout();
    this.failed = false;
  }

  /** @internal */
  public _tryAuth(): void {
    this.authService.login(this.username, this.password)
      .subscribe(() => {
        this.router.navigateByUrl('home');
      }, () => {
        this.failed = true;
      });
  }

}


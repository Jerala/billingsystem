import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ClarityModule} from '@clr/angular';
import {FooterComponent} from './footer.component';

@NgModule({
  declarations: [
    FooterComponent
  ],
  imports: [
    CommonModule,
    ClarityModule
  ],
  exports: [
    FooterComponent
  ]
})
export class FooterModule {
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPhoneNumber'
})
export class FormatPhoneNumberPipe implements PipeTransform {

  transform(phoneNumber: number): string {
    if (!phoneNumber) {
      return null;
    }
    const stringPhoneNumber = phoneNumber.toString();
    if (stringPhoneNumber.length !== 11) {
      return stringPhoneNumber;
    }
    let formattedPhoneNumber: string;
    formattedPhoneNumber = '+' + stringPhoneNumber[0] + ' ('
      + stringPhoneNumber.substring(1, 4)
      + ') ' + stringPhoneNumber.substring(4, 7) + '-' + stringPhoneNumber.substring(7, 9)
      + '-' + stringPhoneNumber.substring(9, 11);
    return formattedPhoneNumber;
  }

}

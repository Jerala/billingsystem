import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormatPhoneNumberPipe} from './format-phone-number.pipe';

@NgModule({
  declarations: [ FormatPhoneNumberPipe ],
  imports: [
    CommonModule
  ],
  exports: [ FormatPhoneNumberPipe ]
})
export class PipeModule { }
